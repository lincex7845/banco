package com.banco.persistencia;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.persistencia.dao.CuentaDAO;
import com.banco.persistencia.entidades.Cuenta;

public class CuentaDAOTest {

	private CuentaDAO cuentaDAO = new CuentaDAO();

	@Test
	public void agregarCuentaTest() {
		try {
			Cuenta c = new Cuenta();
			c.setNumero("10852741-66");
			c.setSaldo(0L);
			cuentaDAO.agregarCuenta(c);
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("numero", OperadoresConsulta.IGUAL, c.getNumero()));
			List<Cuenta> cuentas = (List<Cuenta>) cuentaDAO
					.obtenerCuenta(criterios.toArray(new CriterioConsulta[criterios.size()]));
			if (cuentas.size() == 1) {
				Assert.assertTrue(cuentas.get(0).getNumero().equals(c.getNumero())
						&& cuentas.get(0).getSaldo().equals(c.getSaldo()));
				cuentaDAO.eliminarCuenta(c.getNumero());
			} else
				Assert.fail(String.format("Existen %d coincidecias", cuentas.size()));
		} catch (Exception | ExcepcionRegistroExiste | ExcepcionRegistroNoExiste e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void eliminarCuentaTest() {
		try {
			Cuenta c = new Cuenta();
			c.setNumero("10852741-66");
			c.setSaldo(0L);
			cuentaDAO.agregarCuenta(c);
			cuentaDAO.eliminarCuenta(c.getNumero());
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("numero", OperadoresConsulta.IGUAL, c.getNumero()));
			List<Cuenta> cuentas = (List<Cuenta>) cuentaDAO
					.obtenerCuenta(criterios.toArray(new CriterioConsulta[criterios.size()]));
			if (cuentas.size() > 0) {
				Assert.fail(String.format("Existen %d coincidecias", cuentas.size()));
			} else
				Assert.assertTrue(true);

		} catch (Exception | ExcepcionRegistroExiste | ExcepcionRegistroNoExiste e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test(expected = ExcepcionRegistroExiste.class)
	public void agregarCuentaDuplicadaTest() throws ExcepcionRegistroExiste {
		try
		{
			Cuenta c = new Cuenta();
			c.setNumero("10852741-66");
			c.setSaldo(0L);
			cuentaDAO.agregarCuenta(c);
			Cuenta c1 = new Cuenta();
			c1.setNumero("10852741-66");
			c1.setSaldo(6000L);
			cuentaDAO.agregarCuenta(c1);
			cuentaDAO.eliminarCuenta(c.getNumero());
		}
		catch(Exception | ExcepcionRegistroNoExiste e)
		{
			Assert.fail(e.getMessage());
		}
		catch(ExcepcionRegistroExiste e)
		{
			throw e;
		}
		
	}
}