package com.banco.persistencia;

import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import com.banco.infraestructura.utilidades.persistencia.TipoMovimiento;
import com.banco.persistencia.dao.MovimientoDAO;
import com.banco.persistencia.entidades.Movimiento;

public class MovimientoDAOTest {
	
	private MovimientoDAO movimientoDAO = new MovimientoDAO();

	@Test
	public void testAgregarMovimiento() {
		try
		{
			Movimiento mov1 = new Movimiento();
			mov1.setFechaMovimiento(new Date());
			mov1.setTipoMovimiento(TipoMovimiento.DEBITO);
			mov1.setValor(90L);
			movimientoDAO.agregarMovimiento(mov1);
			Movimiento mov2 = new Movimiento();
			mov2.setFechaMovimiento(new Date());
			mov2.setTipoMovimiento(TipoMovimiento.CREDITO);
			mov2.setValor(90L);
			movimientoDAO.agregarMovimiento(mov2);
			Assert.assertTrue(true);
		}
		catch(Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testObtenerMovimientos() {
		try
		{
			Movimiento mov1 = new Movimiento();
			mov1.setFechaMovimiento(new Date());
			mov1.setTipoMovimiento(TipoMovimiento.DEBITO);
			mov1.setValor(90L);
			movimientoDAO.agregarMovimiento(mov1);
			Movimiento mov2 = new Movimiento();
			mov2.setFechaMovimiento(new Date());
			mov2.setTipoMovimiento(TipoMovimiento.CREDITO);
			mov2.setValor(90L);
			movimientoDAO.agregarMovimiento(mov2);
			List<Movimiento> movimiento = (List<Movimiento>) movimientoDAO.obtenerMovimientos();
			Assert.assertTrue(movimiento.size() > 0);
		}
		catch(Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

}
