package com.banco.persistencia;

import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.persistencia.dao.ClienteDAO;
import com.banco.persistencia.entidades.Cliente;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ClienteDAOTest {
	private ClienteDAO clienteDAO = new ClienteDAO();

	@Test
	public void crearClienteTest() {
		try {
			Cliente nuevo = new Cliente();
			nuevo.setNombre("David Camilo");
			nuevo.setApellido("Mera David");
			nuevo.setIdentificacion("1085274166");
			nuevo.setDireccion("AV siempre viva");
			nuevo.setTelefono("7202020");
			clienteDAO.agregarCliente(nuevo);
			// Buscar Cliente
			Criteria criteria = clienteDAO.obtenerCriterios();
			ArrayList<Criterion> criterios = new ArrayList<Criterion>();
			criterios.add(Restrictions.eq("nombre", nuevo.getNombre()));
			criterios.add(Restrictions.eq("apellido", nuevo.getApellido()));
			criterios.add(Restrictions.eq("identificacion", nuevo.getIdentificacion()));
			List<CriterioConsulta> criterios2 = new ArrayList<CriterioConsulta>();
			criterios2.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, nuevo.getNombre()));
			criterios2.add(new CriterioConsulta("apellido", OperadoresConsulta.IGUAL, nuevo.getApellido()));
			criterios2.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, nuevo.getIdentificacion()));
			for (Criterion c : criterios)
				criteria.add(c);
			Cliente clienteAgregado = (Cliente) criteria.list().get(0);
			// Criterion[] co = criterios.toArray(new
			// Criterion[criterios.size()]);
			Cliente clienteInsertado = (Cliente) clienteDAO
					.buscarClientesPor(criterios2.toArray(new CriterioConsulta[criterios2.size()])).get(0);
			Assert.assertTrue(clienteAgregado.equals(clienteInsertado));
			clienteDAO.eliminarCliente(nuevo);
		} catch (Exception | ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void actualizarClienteTest() {
		try {
			Cliente nuevo = new Cliente();
			nuevo.setNombre("David Camilo");
			nuevo.setApellido("Mera David");
			nuevo.setIdentificacion("1085274166");
			nuevo.setDireccion("AV siempre viva");
			nuevo.setTelefono("7202020");
			clienteDAO.agregarCliente(nuevo);
			// Actualizar
//			Cliente clienteAct = new Cliente();
			nuevo.setNombre("David");
			//clienteAct.setIdentificacion("1085275177");-- NaturalId is inmutable
			nuevo.setApellido("Kan");
			clienteDAO.actualizarCliente(nuevo, nuevo);
			// ArrayList<Criterion> criterios = new ArrayList<Criterion>();
			// criterios.add(Restrictions.eq("apellido",
			// clienteAct.getApellido()));
			// criterios.add(Restrictions.eq("identificacion",
			// clienteAct.getIdentificacion()));
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("apellido", OperadoresConsulta.IGUAL, nuevo.getApellido()));
			criterios.add(
					new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, "1085274166"));
			Cliente clienteActualizado = (Cliente) clienteDAO
					.buscarClientesPor(criterios.toArray(new CriterioConsulta[criterios.size()]))
					.get(0);
			Assert.assertTrue(clienteActualizado.getIdentificacion().equals("1085274166")
					&& clienteActualizado.getApellido().equals(nuevo.getApellido()));
			criterios.clear();
			// criterios.add(Restrictions.eq("apellido", "Mera David"));
			// criterios.add(Restrictions.eq("identificacion", "1085274166"));
			criterios.add(new CriterioConsulta("apellido", OperadoresConsulta.IGUAL, "Mera David"));
			criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, "1085274166"));
			List<Cliente> clientes = (List<Cliente>) clienteDAO
					.buscarClientesPor(criterios.toArray(new CriterioConsulta[criterios.size()]));
			Assert.assertTrue(clientes.size() == 0);
			clienteDAO.eliminarCliente(clienteActualizado);
		} catch (Exception | ExcepcionRegistroExiste | ExcepcionRegistroNoExiste e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void eliminarClienteTest() {
		try {
			Cliente nuevo = new Cliente();
			nuevo.setNombre("David Camilo");
			nuevo.setApellido("Mera David");
			nuevo.setIdentificacion("1085274166");
			nuevo.setDireccion("AV siempre viva");
			nuevo.setTelefono("7202020");
			clienteDAO.agregarCliente(nuevo);
			clienteDAO.eliminarCliente(nuevo);
			// ArrayList<Criterion> criterios = new ArrayList<Criterion>();
			// criterios.add(Restrictions.eq("apellido", nuevo.getApellido()));
			// criterios.add(Restrictions.eq("identificacion",
			// nuevo.getIdentificacion()));
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, nuevo.getNombre()));
			criterios.add(new CriterioConsulta("apellido", OperadoresConsulta.IGUAL, nuevo.getApellido()));
			criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, nuevo.getIdentificacion()));
			List<Cliente> clientes = (List<Cliente>) clienteDAO
					.buscarClientesPor(criterios.toArray(new CriterioConsulta[criterios.size()]));
			Assert.assertTrue(clientes.size() == 0);
		} catch (Exception | ExcepcionRegistroExiste | ExcepcionRegistroNoExiste e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test(expected = ExcepcionRegistroExiste.class)
	public void agregarDuplicado() throws ExcepcionRegistroExiste {
		Cliente nuevo = null;
		try {
			nuevo = new Cliente();
			nuevo.setNombre("David Camilo");
			nuevo.setApellido("Mera David");
			nuevo.setIdentificacion("1085274166");
			nuevo.setDireccion("AV siempre viva");
			nuevo.setTelefono("7202020");
			clienteDAO.agregarCliente(nuevo);

		} catch (Exception | ExcepcionRegistroExiste e) {
			Assert.fail(e.getMessage());
		}
		try {
			nuevo = new Cliente();
			nuevo.setNombre("David");
			nuevo.setApellido("Mera");
			nuevo.setIdentificacion("1085274166");
			nuevo.setDireccion("AV siempre viva");
			nuevo.setTelefono("7202020");
			clienteDAO.agregarCliente(nuevo);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		} catch (ExcepcionRegistroExiste e) {
			throw e;
		} finally {
			try {
				clienteDAO.eliminarCliente(nuevo);
			} catch (ExcepcionRegistroNoExiste e) {
				Assert.fail(e.getMessage());
			}

		}
	}
}