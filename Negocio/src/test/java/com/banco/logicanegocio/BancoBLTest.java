package com.banco.logicanegocio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import org.junit.Test;
import com.banco.infraestructura.excepciones.ExcepcionNegocio;
import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.excepciones.ExcepcionSaldoInsuficiente;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.infraestructura.utilidades.persistencia.TipoMovimiento;

public class BancoBLTest {

	@Test
	public void testCrearCliente() {
		ClienteDTO cliente = new ClienteDTO();
		cliente.setApellido("Mera");
		cliente.setNombre("Julio");
		cliente.setIdentificacion("12256256");
		cliente.setDireccion("avenida siempre viva calle falsa");
		cliente.setTelefono("7500500");
		CuentaDTO cuenta = new CuentaDTO();
		cuenta.setNumeroCuenta("1212-12");
		cuenta.setSaldo(0L);
		MovimientoDTO movimiento1 = new MovimientoDTO();
		movimiento1.setFechaMovimiento(new java.util.Date());
		movimiento1.setTipoMovimiento(TipoMovimiento.DEBITO);
		movimiento1.setValor(800000L);
		MovimientoDTO movimiento2 = new MovimientoDTO();
		movimiento2.setFechaMovimiento(new java.util.Date());
		movimiento2.setTipoMovimiento(TipoMovimiento.CREDITO);
		movimiento2.setValor(200000L);
		cuenta.setMovimientos(asList(movimiento1, movimiento2));
		cliente.setCuentasBancarias(asList(cuenta));
		
		try {
			BancoBL.crearCliente(cliente);
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
			criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));			
			Long idCliente = BancoBL.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()])).get(0)
					.getId();
			BancoBL.eliminarCliente(idCliente);
			assertTrue(true);
		} catch (ExcepcionNegocio e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testConsultarClientes() {
		ClienteDTO cliente = new ClienteDTO();
		cliente.setApellido("Pantoja");
		cliente.setNombre("Julio");
		cliente.setIdentificacion("969860254");
		cliente.setDireccion("avenida siempre viva calle falsa");
		cliente.setTelefono("7500500");
		try {
			BancoBL.crearCliente(cliente);			
		} catch (ExcepcionNegocio e) {
			fail(e.getMessage());
		}
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
		criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
		criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
		List<ClienteDTO> clientes = BancoBL
				.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()]));
		assertTrue(clientes.size() > 0);
		try {
			BancoBL.eliminarCliente(clientes.get(0).getId());
		} catch (ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e) {
			fail(e.getMessage());
		}
		assertTrue(true);
	}

	@Test
	public void testActualizarCliente() {
		ClienteDTO cliente = new ClienteDTO();
		cliente.setApellido("Mera");
		cliente.setNombre("Sophia");
		cliente.setIdentificacion("10147411559");
		cliente.setDireccion("avenida siempre viva calle falsa");
		cliente.setTelefono("7500500");
		try {
			BancoBL.crearCliente(cliente);
		} catch (ExcepcionNegocio e) {
			fail(e.getMessage());
		}
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
		criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
		criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
		List<ClienteDTO> clientes = BancoBL
				.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()]));
		ClienteDTO clienteActual = clientes.get(0);
		cliente.setApellido("Mera Eraso");
		cliente.setNombre("Sofia");
		cliente.setIdentificacion("10147411559");
		cliente.setDireccion("avenida siempre viva calle falsa verbenal");
		cliente.setTelefono("7500500-2");
		try {
			BancoBL.actualizarCliente(clienteActual, clienteActual.getId());
			BancoBL.eliminarCliente(clienteActual.getId());
		} catch (ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e) {
			fail(e.getMessage());
		}
		assertTrue(true);
	}

	@Test
	public void testEliminarCliente() {
		List<ClienteDTO> clientes = BancoBL.consultarClientes();
		Long idCliente = 0L;
		if (clientes.size() > 0) {
			idCliente = clientes.get(0).getId();
		} else {
			ClienteDTO cliente = new ClienteDTO();
			cliente.setApellido("Ronaldo");
			cliente.setNombre("Cristiano");
			cliente.setIdentificacion("7");
			cliente.setDireccion("madrir");
			cliente.setTelefono("cr7");
			try {
				BancoBL.crearCliente(cliente);
			} catch (ExcepcionNegocio e) {
				fail(e.getMessage());
			}
			idCliente = BancoBL.consultarClientes().get(0).getId();
		}
		try {
			BancoBL.eliminarCliente(idCliente);
		} catch (ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e) {
			fail(e.getMessage());
		}
		assertTrue(true);
	}

	@Test
	public void testEliminarClienteComplejo() {
		ClienteDTO cliente = new ClienteDTO();
		cliente.setApellido("Mera");
		cliente.setNombre("Camilo");
		cliente.setIdentificacion("89042751643");
		cliente.setDireccion("avenida siempre viva calle falsa");
		cliente.setTelefono("7500500");
		CuentaDTO cuenta = new CuentaDTO();
		cuenta.setNumeroCuenta("1212-12");
		cuenta.setSaldo(60000L);
		MovimientoDTO movimiento1 = new MovimientoDTO();
		movimiento1.setFechaMovimiento(new java.util.Date());
		movimiento1.setTipoMovimiento(TipoMovimiento.DEBITO);
		movimiento1.setValor(800000L);
		MovimientoDTO movimiento2 = new MovimientoDTO();
		movimiento2.setFechaMovimiento(new java.util.Date());
		movimiento2.setTipoMovimiento(TipoMovimiento.CREDITO);
		movimiento2.setValor(200000L);
		cuenta.setMovimientos(asList(movimiento1, movimiento2));
		cliente.setCuentasBancarias(asList(cuenta));
		try {
			BancoBL.crearCliente(cliente);
		} catch (ExcepcionNegocio e) {
			fail(e.getMessage());
		}
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
		criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
		criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
		Long idCliente = BancoBL.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()])).get(0)
				.getId();
		try {
			BancoBL.eliminarCliente(idCliente);
		} catch (ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e) {
			fail(e.getMessage());
		}
		assertTrue(true);
	}

	@Test
	public void testEliminarCuenta() {
		ClienteDTO cliente = new ClienteDTO();
		cliente.setApellido("Mera");
		cliente.setNombre("Camilo");
		cliente.setIdentificacion("89030324689");
		cliente.setDireccion("avenida siempre viva calle falsa");
		cliente.setTelefono("7500500");
		// ---
		CuentaDTO cuenta = new CuentaDTO();
		cuenta.setNumeroCuenta("07569841-23");
		cuenta.setSaldo(100000L);
		// *
		CuentaDTO cuenta2 = new CuentaDTO();
		cuenta2.setNumeroCuenta("07569841-24");
		cuenta2.setSaldo(0L);
		// ---
		MovimientoDTO movimiento3 = new MovimientoDTO();
		movimiento3.setFechaMovimiento(new java.util.Date());
		movimiento3.setTipoMovimiento(TipoMovimiento.CREDITO);
		movimiento3.setValor(800000L);
		// *
		MovimientoDTO movimiento4 = new MovimientoDTO();
		movimiento4.setFechaMovimiento(new java.util.Date());
		movimiento4.setTipoMovimiento(TipoMovimiento.CREDITO);
		movimiento4.setValor(200000L);
		// *
		MovimientoDTO movimiento1 = new MovimientoDTO();
		movimiento1.setFechaMovimiento(new java.util.Date());
		movimiento1.setTipoMovimiento(TipoMovimiento.DEBITO);
		movimiento1.setValor(800000L);
		// *
		MovimientoDTO movimiento2 = new MovimientoDTO();
		movimiento2.setFechaMovimiento(new java.util.Date());
		movimiento2.setTipoMovimiento(TipoMovimiento.CREDITO);
		movimiento2.setValor(200000L);
		// --
		cuenta.setMovimientos(asList(movimiento1, movimiento2));
		cuenta2.setMovimientos(asList(movimiento3, movimiento4));
		cliente.setCuentasBancarias(asList(cuenta, cuenta2));
		try {
			BancoBL.crearCliente(cliente);
		} catch (ExcepcionNegocio e) {
			fail(e.getMessage());
		}
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
		criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
		criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
		ClienteDTO c = BancoBL.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()])).get(0);
		for (CuentaDTO cu : c.getCuentasBancarias()) {
			try {
				BancoBL.eliminarCuenta(c.getId(), cu.getNumeroCuenta());
			} catch (ExcepcionRegistroNoExiste | ExcepcionRegistroExiste | CloneNotSupportedException e) {
				fail(e.getMessage());
			}
		}
		try {
			BancoBL.eliminarCliente(c.getId());
		} catch (ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e) {
			fail(e.getMessage());
		}
		assertTrue(true);
	}

	@Test
	public void testAgregarMovimiento() {
		try
		{
			ClienteDTO cliente = new ClienteDTO();
			cliente.setApellido("Mera");
			cliente.setNombre("Camilo");
			cliente.setIdentificacion("89030324689");
			cliente.setDireccion("avenida siempre viva calle falsa");
			cliente.setTelefono("7500500");
			// *
			CuentaDTO cuenta2 = new CuentaDTO();
			cuenta2.setNumeroCuenta("07569841-24");
			cuenta2.setSaldo(0L);
			MovimientoDTO movimiento4 = new MovimientoDTO();
			movimiento4.setFechaMovimiento(new java.util.Date());
			movimiento4.setTipoMovimiento(TipoMovimiento.CREDITO);
			movimiento4.setValor(200000L);
			// *
			MovimientoDTO movimiento1 = new MovimientoDTO();
			movimiento1.setFechaMovimiento(new java.util.Date());
			movimiento1.setTipoMovimiento(TipoMovimiento.DEBITO);
			movimiento1.setValor(800000L);
			// *
			MovimientoDTO movimiento2 = new MovimientoDTO();
			movimiento2.setFechaMovimiento(new java.util.Date());
			movimiento2.setTipoMovimiento(TipoMovimiento.CREDITO);
			movimiento2.setValor(200000L);
			// *
			//cuenta2.setMovimientos(asList(movimiento1, movimiento2, movimiento4));
			cliente.setCuentasBancarias(asList(cuenta2));
			try {
				BancoBL.crearCliente(cliente);
			} catch (ExcepcionNegocio e) {
				fail(e.getMessage());
			}
			BancoBL.agregarMovimiento(cuenta2.getNumeroCuenta(), movimiento1);
			BancoBL.agregarMovimiento(cuenta2.getNumeroCuenta(), movimiento2);
			BancoBL.agregarMovimiento(cuenta2.getNumeroCuenta(), movimiento4);
			// 0 + 800.000 - 200.000 - 200.000 = 400.000
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
			criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
			List<ClienteDTO> clientes = BancoBL
					.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()]));
			assertTrue(clientes.get(0).getCuentasBancarias().get(0).getSaldo().equals(400000L));
			BancoBL.eliminarCliente(clientes.get(0).getId());
			assertTrue(true);
		}
		catch(Exception | ExcepcionRegistroNoExiste | ExcepcionRegistroExiste | ExcepcionSaldoInsuficiente e)
		{
			fail(e.getMessage());
		}
	}
	
	@Test(expected = ExcepcionSaldoInsuficiente.class)
	public void testAgregarMovimiento_SaldoInsuficiente() throws ExcepcionSaldoInsuficiente
	{
		try
		{
			ClienteDTO cliente = new ClienteDTO();
			cliente.setApellido("Mera");
			cliente.setNombre("Camilo");
			cliente.setIdentificacion("89030324689");
			cliente.setDireccion("avenida siempre viva calle falsa");
			cliente.setTelefono("7500500");
			// *
			CuentaDTO cuenta2 = new CuentaDTO();
			cuenta2.setNumeroCuenta("07569841-24");
			cuenta2.setSaldo(0L);
			MovimientoDTO movimiento4 = new MovimientoDTO();
			movimiento4.setFechaMovimiento(new java.util.Date());
			movimiento4.setTipoMovimiento(TipoMovimiento.CREDITO);
			movimiento4.setValor(800000L);
			// *
			MovimientoDTO movimiento1 = new MovimientoDTO();
			movimiento1.setFechaMovimiento(new java.util.Date());
			movimiento1.setTipoMovimiento(TipoMovimiento.DEBITO);
			movimiento1.setValor(800000L);
			// *
			MovimientoDTO movimiento2 = new MovimientoDTO();
			movimiento2.setFechaMovimiento(new java.util.Date());
			movimiento2.setTipoMovimiento(TipoMovimiento.CREDITO);
			movimiento2.setValor(200000L);
			// *
			//cuenta2.setMovimientos(asList(movimiento1, movimiento2, movimiento4));
			cliente.setCuentasBancarias(asList(cuenta2));
			try {
				BancoBL.crearCliente(cliente);
			} catch (ExcepcionNegocio e) {
				fail(e.getMessage());
			}
			BancoBL.agregarMovimiento(cuenta2.getNumeroCuenta(), movimiento1);
			BancoBL.agregarMovimiento(cuenta2.getNumeroCuenta(), movimiento2);
			BancoBL.agregarMovimiento(cuenta2.getNumeroCuenta(), movimiento4);
			// 0 + 800.000 - 200.000 - 200.000 = 400.000
			List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
			criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
			criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
			List<ClienteDTO> clientes = BancoBL
					.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()]));
			assertTrue(clientes.get(0).getCuentasBancarias().get(0).getSaldo().equals(400000L));
			BancoBL.eliminarCliente(clientes.get(0).getId());
			assertTrue(true);
		}
		catch(Exception | ExcepcionRegistroNoExiste | ExcepcionRegistroExiste e)
		{
			fail(e.getMessage());
		} catch (ExcepcionSaldoInsuficiente e) {
			throw e;
		}
	}

}
