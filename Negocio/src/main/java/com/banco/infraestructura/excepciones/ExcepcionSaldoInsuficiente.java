package com.banco.infraestructura.excepciones;

public class ExcepcionSaldoInsuficiente extends ExcepcionNegocio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9136621198640733858L;

	public ExcepcionSaldoInsuficiente(String errorMessage, Throwable e) {
		super(errorMessage, e);
	}
	
	public ExcepcionSaldoInsuficiente(String errorMessage) {
		super(errorMessage);
	}
}
