/**
 * 
 */
package com.banco.infraestructura.excepciones;

/**
 * @author david.mera
 *
 */
public class ExcepcionRegistroExiste extends ExcepcionNegocio {

	public ExcepcionRegistroExiste(String errorMessage, Throwable e) {
		super(errorMessage, e);
	}
	
	public ExcepcionRegistroExiste(String errorMessage) {
		super(errorMessage);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4090005774832174603L;

}
