package com.banco.infraestructura.excepciones;

public class ExcepcionNegocio extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1543869466788472819L;
	
	public ExcepcionNegocio(String errorMessage) {
		super(errorMessage);
	}
	
	public ExcepcionNegocio(String errorMessage, Throwable e) {
		super(errorMessage,e);
	}
}
