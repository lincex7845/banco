/**
 * 
 */
package com.banco.infraestructura.excepciones;

/**
 * @author david.mera
 *
 */
public class ExcepcionRegistroNoExiste extends ExcepcionNegocio {

	public ExcepcionRegistroNoExiste(String errorMessage, Throwable e) {
		super(errorMessage, e);
	}
	
	public ExcepcionRegistroNoExiste(String errorMessage) {
		super(errorMessage);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4090005774832174603L;
}
