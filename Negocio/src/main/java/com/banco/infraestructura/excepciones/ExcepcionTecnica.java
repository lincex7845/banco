package com.banco.infraestructura.excepciones;

public class ExcepcionTecnica extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 965382829383233772L;
	
	public ExcepcionTecnica(String errorMessage) {
		super(errorMessage);
	}
	
	public ExcepcionTecnica(String errorMessage, Throwable e) {
		super(errorMessage,e);
	}

}
