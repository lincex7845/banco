package com.banco.infraestructura.utilidades.validadores;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator {

	private final static String PATRONURL = "(https?)://[-a-zA-Z0-9?+&@#/%?=~_|!:.]*";

	public static boolean isValidString(String string) {

		return (string != null) && (!string.isEmpty());
	}

	/***
	 * Valida si una cadena es URL
	 * 
	 * @param string
	 * @param pattern
	 * @return
	 */
	public static boolean isURL(String string) {
		try {
			Pattern p = Pattern.compile(PATRONURL);
			Matcher m = p.matcher(string);
			return m.matches();
		} catch (RuntimeException ex) {
			return false;
		}
	}
}
