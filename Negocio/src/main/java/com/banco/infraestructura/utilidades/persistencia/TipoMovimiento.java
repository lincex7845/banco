/**
 * 
 */
package com.banco.infraestructura.utilidades.persistencia;

/**
 * @author david.mera
 *
 */
public enum TipoMovimiento {

	DEBITO ,
	CREDITO;
}
