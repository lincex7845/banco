package com.banco.infraestructura.utilidades.persistencia;

public class CriterioConsulta {

	private String nombreAtributo;
	private OperadoresConsulta operador;
	private Object valor;
	
	public CriterioConsulta(){
		
	}
	
	public CriterioConsulta(String nombreAtributo, OperadoresConsulta operador, Object valor)
	{
		this.nombreAtributo = nombreAtributo;
		this.operador = operador;
		this.valor = valor;
	}
	
	/**
	 * @return the nombreAtributo
	 */
	public String getNombreAtributo() {
		return nombreAtributo;
	}
	/**
	 * @param nombreAtributo the nombreAtributo to set
	 */
	public void setNombreAtributo(String nombreAtributo) {
		this.nombreAtributo = nombreAtributo;
	}
	/**
	 * @return the operador
	 */
	public OperadoresConsulta getOperador() {
		return operador;
	}
	/**
	 * @param operador the operador to set
	 */
	public void setOperador(OperadoresConsulta operador) {
		this.operador = operador;
	}
	/**
	 * @return the valor
	 */
	public Object getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Object valor) {
		this.valor = valor;
	}
	
}
