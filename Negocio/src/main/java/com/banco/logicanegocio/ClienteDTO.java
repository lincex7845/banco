package com.banco.logicanegocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.banco.persistencia.entidades.Cliente;
import com.banco.persistencia.entidades.Cuenta;

public class ClienteDTO {

	private Long id;
	private String nombre;
	private String apellido;
	private String identificacion;
	private String direccion;
	private String telefono;
	private List<CuentaDTO> cuentasBancarias;
	
	public ClienteDTO(){
		
	}
	
	public ClienteDTO(Long id, String nombre, String apellido, String identificacion, String direccion, String telefono, List<CuentaDTO> cuentas)
	{
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.identificacion = identificacion;
		this.direccion = direccion;
		this.telefono = telefono;
		this.cuentasBancarias = cuentas;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the identificacion
	 */
	public String getIdentificacion() {
		return identificacion;
	}
	/**
	 * @param identificacion the identificacion to set
	 */
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}
	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the cuentasBancarias
	 */
	public List<CuentaDTO> getCuentasBancarias() {
		return cuentasBancarias;
	}
	/**
	 * @param cuentasBancarias the cuentasBancarias to set
	 */
	public void setCuentasBancarias(List<CuentaDTO> cuentasBancarias) {
		this.cuentasBancarias = cuentasBancarias;
	}
	
	public Cliente toCliente()
	{
		Set<Cuenta> cuentas = null;
		if(this.cuentasBancarias != null && cuentasBancarias.size() > 0)
		{
			cuentas = new HashSet<Cuenta>();
			for(int i = 0; i < cuentasBancarias.size(); i++)
				cuentas.add(cuentasBancarias.get(i).toCuenta());
		}
		return new Cliente(id,nombre, apellido, identificacion, direccion, telefono, cuentas);
	}
	
	public ClienteDTO (Cliente cliente)
	{
		id = cliente.getId();
		nombre = cliente.getNombre();
		apellido = cliente.getApellido();
		identificacion = cliente.getIdentificacion();
		direccion = cliente.getDireccion();
		telefono = cliente.getTelefono();
		cuentasBancarias = new ArrayList<CuentaDTO>();
		for(Cuenta c : cliente.getCuentas())
		{
			cuentasBancarias.add(new CuentaDTO(c));
		}		
	}
}
