package com.banco.logicanegocio;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.excepciones.ExcepcionSaldoInsuficiente;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.infraestructura.utilidades.persistencia.TipoMovimiento;
import com.banco.persistencia.dao.ClienteDAO;
import com.banco.persistencia.dao.CuentaDAO;
import com.banco.persistencia.entidades.Cliente;
import com.banco.persistencia.entidades.Cuenta;
import com.banco.persistencia.entidades.Movimiento;

public class BancoBL {
	
	/***
	 * 
	 * @param nuevoCliente
	 * @throws ExcepcionRegistroExiste
	 * @throws IllegalArgumentException
	 */
	public static void crearCliente(ClienteDTO nuevoCliente) throws ExcepcionRegistroExiste 
	{
		// Validaciones
		if(nuevoCliente.getCuentasBancarias() != null && nuevoCliente.getCuentasBancarias().size() == 0)
			throw new IllegalArgumentException("Se requiere al menos una cuenta bancaria para crear el cliente.");
		ClienteDAO clienteDAO = new ClienteDAO();
		clienteDAO.agregarCliente(nuevoCliente.toCliente());
	}
	
	public static List<ClienteDTO> consultarClientes(CriterioConsulta... criterios)
	{
		List<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
		ClienteDAO clienteDAO = new ClienteDAO();
		List<Cliente> clientesBD = clienteDAO.buscarClientesPor(criterios);
		//Set<Cliente> clientesUnico = new HashSet<Cliente>(clienteDAO.buscarClientesPor(criterios));
		for(int i = 0; i < clientesBD.size(); i++)
		{
			clientes.add(new ClienteDTO(clientesBD.get(i)));
		}
		return clientes;
	}
	
	public static void actualizarCliente(ClienteDTO clienteActual, Long idCliente) throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste
	{
		ClienteDAO clienteDAO = new ClienteDAO();
		Cliente clienteViejo = clienteDAO.obtenerClientePorId(idCliente);
		clienteDAO.actualizarCliente(clienteViejo, clienteActual.toCliente());
	}
	
	public static void eliminarCliente(Long idCliente) throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste
	{
		ClienteDAO clienteDAO = new ClienteDAO();
		Cliente clienteViejo = clienteDAO.obtenerClientePorId(idCliente);
		clienteDAO.eliminarCliente(clienteViejo);
		// Eliminar cuentas y movimientos en cascada
//		Set<Cuenta> cuentas = clienteViejo.getCuentas();
//		CuentaDAO cuentaDAO = new CuentaDAO();
//		for(Cuenta c : cuentas)
//		{
//			try {
//				cuentaDAO.eliminarCuenta(c.getNumero());
//			} catch (ExcepcionRegistroNoExiste e) {
//				//Eliminada
//				e.printStackTrace();
//			} catch (ExcepcionRegistroExiste e) {
//				throw e;
//			}
//		}
	}
	
	public static void eliminarCuenta(Long idCliente, String numeroCuenta) throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste, CloneNotSupportedException 
			//throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste
	{
		ClienteDAO clienteDAO = new ClienteDAO();
		Cliente cliente = clienteDAO.obtenerClientePorId(idCliente);
		//Cliente clienteNuevo = (Cliente) clienteViejo.clone();
				//ClienteDAO.cargarCuentasLazy(clienteNuevo);
		Set<Cuenta> cuentas = cliente.getCuentas();
		for(Cuenta c : cuentas)
			if(c.getNumero().equals(numeroCuenta))
			{
				cuentas.remove(c);
				break;
			}
		cliente.setCuentas(cuentas);
		// para evitar duplicados se desactiva lock
		clienteDAO.actualizarCliente(cliente, cliente);
		// Comprobar cuenta
		CuentaDAO cuentaDAO = new CuentaDAO();		
		try {
			cuentaDAO.eliminarCuenta(numeroCuenta);
		} catch (ExcepcionRegistroNoExiste e) {
			// Eliminada
			e.printStackTrace();
		}
	}
	
	public static void agregarMovimiento(String numeroCuenta, MovimientoDTO movimiento) throws ExcepcionSaldoInsuficiente
	{
//		MovimientoDAO movimientoDAO = new MovimientoDAO();
//		movimientoDAO.agregarMovimiento(movimiento.toMovimiento());
		// Validacion	
		List<CriterioConsulta>criterios = new ArrayList<CriterioConsulta>();
		criterios.add(new CriterioConsulta("numero", OperadoresConsulta.IGUAL, numeroCuenta));
		CuentaDAO cuentaDAO = new CuentaDAO();	
		Cuenta c = cuentaDAO.obtenerCuenta(criterios.toArray(new CriterioConsulta[criterios.size()])).get(0);
		long nuevoSaldo = movimiento.getTipoMovimiento() == TipoMovimiento.CREDITO ? c.getSaldo() - movimiento.getValor() : c.getSaldo() + movimiento.getValor();
		if(nuevoSaldo < 0)
			throw new ExcepcionSaldoInsuficiente("El cliente no posee saldo suficiente para realizar este movimiento.");
		c.setSaldo(nuevoSaldo);
		Set<Movimiento> movimientos = c.getMovimientos();
		movimientos.add(movimiento.toMovimiento());
		c.setMovimientos(movimientos);
		cuentaDAO.actualizarCuenta(c.getId() , c);
	}

}
