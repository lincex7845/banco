package com.banco.logicanegocio;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.banco.persistencia.entidades.Cuenta;
import com.banco.persistencia.entidades.Movimiento;

public class CuentaDTO {

	private Long id;
	private String numeroCuenta;
	private Long saldo;
	private List<MovimientoDTO> movimientos;

	public CuentaDTO() {
	}
	
	public CuentaDTO(Long id, String numeroCuenta, Long saldo, List<MovimientoDTO> movimientos)
	{
		this.id = id;
		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
		this.movimientos = movimientos;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta
	 *            the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return the saldo
	 */
	public Long getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the movimientos
	 */
	public List<MovimientoDTO> getMovimientos() {
		return movimientos;
	}

	/**
	 * @param movimientos
	 *            the movimientos to set
	 */
	public void setMovimientos(List<MovimientoDTO> movimientos) {
		this.movimientos = movimientos;
	}

	public Cuenta toCuenta() {
		Set<Movimiento> movimientos = null;
		if (this.movimientos != null && this.movimientos.size() > 0) {
			movimientos = new HashSet<Movimiento>();
			for (int i = 0; i < this.movimientos.size(); i++) {
				movimientos.add(this.movimientos.get(i).toMovimiento());
			}
		}
		return new Cuenta(id, numeroCuenta, saldo, movimientos);
	}

	public CuentaDTO(Cuenta cuenta) {
		id = cuenta.getId();
		numeroCuenta = cuenta.getNumero();
		saldo = cuenta.getSaldo();
	}
}
