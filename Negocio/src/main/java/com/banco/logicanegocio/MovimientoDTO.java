package com.banco.logicanegocio;

import java.util.Date;

import com.banco.infraestructura.utilidades.persistencia.TipoMovimiento;
import com.banco.persistencia.entidades.Movimiento;

public class MovimientoDTO {

	private Long id;
	private Date fechaMovimiento;
	private TipoMovimiento tipoMovimiento;
	private Long valor;
	
	public MovimientoDTO()
	{
		
	}
	
	public MovimientoDTO(Long id, Date fechaMovimiento, TipoMovimiento tipo, Long valor)
	{
		this.id = id;
		this.fechaMovimiento = fechaMovimiento;
		this.tipoMovimiento = tipo;
		this.valor = valor;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the fechaMovimiento
	 */
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	/**
	 * @param fechaMovimiento the fechaMovimiento to set
	 */
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	/**
	 * @return the tipoMovimiento
	 */
	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}
	/**
	 * @param tipoMovimiento the tipoMovimiento to set
	 */
	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	/**
	 * @return the valor
	 */
	public Long getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Long valor) {
		this.valor = valor;
	}
	
	public Movimiento toMovimiento()
	{
		return new Movimiento(id, fechaMovimiento, valor, tipoMovimiento);
	}
}