/**
 * 
 */
package com.banco.persistencia.entidades;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.Hibernate;
import org.hibernate.annotations.NaturalId;

/**
 * @author david.mera
 *
 */
@Entity
@Table( name = "CUENTAS" )
public class Cuenta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6197735448432617430L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NaturalId
	private String numero;
	@Min(0)
	private Long saldo;
	// TODO :  usar lazy cuando se resuelva carga proxy correctamente
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	//@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<Movimiento> movimientos;
//	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
//	private Cliente cliente;
	
	public Cuenta(){
		if(!Hibernate.isInitialized(movimientos))
			Hibernate.initialize(movimientos);
	}
	
	/***
	 * 
	 * @param id
	 * @param numero
	 * @param saldo
	 * @param movimientos
	 */
	public Cuenta(
			Long id, String numero, Long saldo, Set<Movimiento> movimientos
			)
	{
		this.id = id;
		this.numero = numero;
		this.saldo = saldo;
		this.movimientos = movimientos;
		if(!Hibernate.isInitialized(movimientos))
			Hibernate.initialize(movimientos);
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return the saldo
	 */
	public Long getSaldo() {
		return saldo;
	}
	/**
	 * @param saldo the saldo to set
	 */
	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the movimiento
	 */
	public Set<Movimiento> getMovimientos() {
		return movimientos;
	}

	/**
	 * @param movimiento the movimiento to set
	 */
	public void setMovimientos(Set<Movimiento> movimiento) {
		this.movimientos = movimiento;
	}

//	/**
//	 * @return the cliente
//	 */
//	public Cliente getCliente() {
//		return cliente;
//	}
//
//	/**
//	 * @param cliente the cliente to set
//	 */
//	public void setCliente(Cliente cliente) {
//		this.cliente = cliente;
//	}
	
}
