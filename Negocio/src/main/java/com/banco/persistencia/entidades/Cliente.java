package com.banco.persistencia.entidades;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author david.mera
 *
 */
@Entity
@Table( name = "CLIENTES" )
public class Cliente implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4960553210592451077L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	private String nombre;
	@NotEmpty
	private String apellido;
	@NaturalId
	@NotEmpty
	private String identificacion;
	@NotEmpty
	private String direccion;
	@NotEmpty
	private String telefono;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<Cuenta> cuentas;
	
	public Cliente(){}
	
	/***
	 * 
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param identificacion
	 * @param direccion
	 * @param telefono
	 * @param cuentas
	 */
	public Cliente(
			Long id, String nombre, String apellido, String identificacion, 
			String direccion, String telefono, Set<Cuenta> cuentas
			)
	{
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.identificacion = identificacion;
		this.cuentas = cuentas;
		this.direccion = direccion;
		this.telefono =  telefono;
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}
	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	/**
	 * @return the identificacion
	 */
	public String getIdentificacion() {
		return identificacion;
	}
	/**
	 * @param identificacion the identificacion to set
	 */
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj)
	{		
		if(obj.getClass() != this.getClass())
			return false;
		boolean nulo = this == null && obj == null;
		if(nulo)
			return true;
		else
		{
			Cliente other = (Cliente)obj;
			if(this.id != other.getId())
				return false;
			if(this.apellido != other.getApellido())
				return false;
			if(this.identificacion != other.getIdentificacion())
				return false;
			if(this.nombre != other.getNombre())
				return false;
			if(this.hashCode() != other.hashCode())
				return false;
			return true;
		}
	}
	
	@Override
	public int hashCode()
	{
		int hashCode = 7;
		hashCode = hashCode * 49 + this.apellido.hashCode();
		hashCode = hashCode * 49 + this.identificacion.hashCode();
		hashCode = hashCode * 49 + this.nombre.hashCode();
		return hashCode;
	}

	/**
	 * @return the cuentas
	 */
	public Set<Cuenta> getCuentas() {		
		return cuentas;
	}

	/**
	 * @param cuentas the cuentas to set
	 */
	public void setCuentas(Set<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		Cliente resultado = (Cliente)super.clone();
		return resultado;
	}
}
