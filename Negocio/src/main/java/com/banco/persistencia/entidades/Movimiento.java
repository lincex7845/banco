package com.banco.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;

//import javax.persistence.CascadeType;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.banco.infraestructura.utilidades.persistencia.TipoMovimiento;

@Entity
@Table( name = "MOVIMIENTOS" )
public class Movimiento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3577655737164811762L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date fechaMovimiento;
	@Min(1)
	private Long valor;
	private TipoMovimiento tipoMovimiento;
//	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
//	private Cuenta cuenta;
	
	public Movimiento(){}
	
	/***
	 * 
	 * @param id
	 * @param fechaMovimiento
	 * @param valor
	 * @param tipoMovimiento
	 */
	public Movimiento(
			Long id, Date fechaMovimiento, Long valor, TipoMovimiento tipoMovimiento
//			, Cuenta cuenta
			)
	{
		this.id = id;
		this.fechaMovimiento = fechaMovimiento;
		this.valor = valor;
		this.tipoMovimiento = tipoMovimiento;
//		this.cuenta = cuenta;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the fechaMovimiento
	 */
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	/**
	 * @param fechaMovimiento the fechaMovimiento to set
	 */
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	/**
	 * @return the valor
	 */
	public Long getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Long valor) {
		this.valor = valor;
	}
	/**
	 * @return the tipoMovimiento
	 */
	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}
	/**
	 * @param tipoMovimiento the tipoMovimiento to set
	 */
	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
//	/**
//	 * @return the cuenta
//	 */
//	public Cuenta getCuenta() {
//		return cuenta;
//	}
//	/**
//	 * @param cuenta the cuenta to set
//	 */
//	public void setCuenta(Cuenta cuenta) {
//		this.cuenta = cuenta;
//	}
//	
}
