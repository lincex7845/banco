package com.banco.persistencia.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ContextoFactory {

	final private static SessionFactory sessionFactory;

    static {
        try {
            // create the sessionfactory from standard config file
            sessionFactory = new Configuration().configure("/hibernate.cfg.xml")
                    .buildSessionFactory();

        } catch (Throwable ex) {
            // log the exception
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Returns the current SessionFactory
     * 
     * @return Current SessionFactory
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
