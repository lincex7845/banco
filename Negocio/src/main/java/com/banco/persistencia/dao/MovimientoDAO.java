/**
 * 
 */
package com.banco.persistencia.dao;

import java.util.List;

import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.persistencia.entidades.Movimiento;

/**
 * @author david.mera
 *
 */
public class MovimientoDAO extends DAO<Movimiento> {

	public MovimientoDAO()
	{
		super(Movimiento.class);
	}
	
	public void agregarMovimiento(Movimiento movimiento)
	{
		this.agregarOrActualizar(movimiento);
	}
	
	public List<Movimiento> obtenerMovimientos(CriterioConsulta... criterios)
	{
		return this.buscarPor(criterios);
	}
}
