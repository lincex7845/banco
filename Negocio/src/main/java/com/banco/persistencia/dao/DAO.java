/**
 * 
 */
package com.banco.persistencia.dao;

import java.util.ArrayList;
import java.util.HashSet;
//import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.stat.Statistics;

import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;

/**
 * @author david.mera
 *
 */
public abstract class DAO <T> {
	
	private Class<T> entidad; 
	
	protected static Statistics stats = statistics();
	
	/**
     * Ensures that every client gets his correct session
     */
    private static final ThreadLocal<Session> sessions = new ThreadLocal<>();

    /**
     * Returns the current hibernate session. Also takes care that there's
     * always an open hibernate transaction when needed.
     * 
     * @return Current hibernate session
     */
    protected static Session getSession() {
        Session result = sessions.get();
        if (result == null) {
            result = ContextoFactory.getSessionFactory().openSession();
            sessions.set(result);
            result.beginTransaction();
        }
        return result;
    }

    /**
     * Closes the current hibernate session, if there is one.
     */
    protected static void closeSession() {
        Session sess = sessions.get();
        if (sess == null || !sess.isOpen())
            return;
        sessions.remove();

        try {
            Throwable error = null;
            try {
//                if (sess.getTransaction().isActive()) {
//                    sess.getTransaction().commit();
//                }
            	sess.getTransaction().commit();
            } catch (Throwable e) {
                sess.getTransaction().rollback();
                error = e;
            } finally {
                try {
                    System.out.println("Starting to close session at: "
                            + stats.getSessionOpenCount());
                    sess.close();
                    System.out.println("Finished session at: "
                            + stats.getSessionCloseCount());
                } catch (Throwable th) {
                    if (error != null) {
                        error.addSuppressed(th);
                    } else {
                        throw th;
                    }
                }
            }
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }
	
	protected static Statistics statistics() {
        Statistics stats = ContextoFactory.getSessionFactory().getStatistics();
        stats.setStatisticsEnabled(true);
        return stats;
    }
	
	protected DAO(Class<T> entidad)
	{
//		this.setEntidad((Class<T>) ((ParameterizedType) getClass()  
//				.getGenericSuperclass()).getActualTypeArguments()[0]); 
		this.setEntidad((Class<T>) entidad);
	}

	/***
	 * 
	 * @param id
	 * @param lock
	 * @return
	 * @deprecated ("Usa proxies y en caso de no coincidencias no retornaria null") usar {@link #buscarPorId(Long)}
	 */
	@Deprecated
	protected T obtenerPorId(Long id, boolean lock)
	{
		T entidad = null;
		if(lock)
			entidad = (T)getSession().load(getEntidad(), id, LockMode.PESSIMISTIC_WRITE);
		else
			entidad = (T)getSession().load(getEntidad(), id);
		closeSession();
		return entidad;
	}
	
	protected T buscarPorId(Long id)
	{
		T entidad = null;
		entidad = (T)getSession().get(getEntidad(), id);
		closeSession();
		return entidad; 
	}
	
	protected List<T> obtenerTodos()
	{
		return buscarPor();
	}
	
//	@SuppressWarnings("unchecked")
//	protected List<T> buscarPor(Criterion... criterion)
//	{
//		Criteria crit = getSession().createCriteria(getEntidad());  
//		for (Criterion c : criterion) {  
//            crit.add(c);  
//        } 
//		List<T> list = crit.list();
//		closeSession();
//		return list;
//	}
	
	
	
	/***
	 * 
	 * @param entidad
	 * @throws IllegalArgumentException
	 */
	protected void agregarOrActualizar(T entidad)
	{
		if(entidad == null)
			throw new IllegalArgumentException("El registro a insertar no puede ser nulo o vacío");
		getSession().saveOrUpdate(entidad);
		closeSession();
	}
	
	protected void eliminar(T entidad)
	{
		if(entidad == null)
			throw new IllegalArgumentException("El registro a eliminar no puede ser nulo o vacío");
		getSession().delete(entidad);
		closeSession();
	}
	
	/**
	 * @return the entidad
	 */
 	protected Class<T> getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad the entidad to set
	 */
	private void setEntidad(Class<T> entidad) {
		this.entidad = entidad;
	}

	protected List<T> buscarPor(CriterioConsulta... criterios)
	{
		Criteria crit = getSession().createCriteria(getEntidad());
		for(CriterioConsulta criterio : criterios)
		{
			OperadoresConsulta operador = criterio.getOperador();
			if(operador == OperadoresConsulta.ORDENAR_POR_ASCENDENTEMENTE ||
					operador == OperadoresConsulta.ORDENAR_POR_DESCENDENTEMENTE)
				crit.addOrder(convertirAOrder(criterio));
			else
				crit.add(convertirACriterion(criterio));
		}
		@SuppressWarnings("unchecked")
		List<T> list = crit.list();
		Set<T> unique = new HashSet<T>(list);
		closeSession();
		List<T> trustedList = new ArrayList<T>();
		trustedList.addAll(unique);
		return trustedList;
	}
	
	protected Criterion convertirACriterion(CriterioConsulta criterio)
	{
		Criterion criterion = null;		
		OperadoresConsulta operador = criterio.getOperador();
		switch (operador)
		{
			case IGUAL:
				criterion = Restrictions.eq(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case DIFERENTE:
				criterion = Restrictions.ne(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case MAYOR_QUE:				
				criterion = Restrictions.gt(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case MAYOR_IGUAL_QUE:
				criterion = Restrictions.ge(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case MENOR_QUE:				
				criterion = Restrictions.lt(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case MENOR_IGUAL_QUE:
				criterion = Restrictions.le(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case CONTIENE:
				criterion = Restrictions.ilike(criterio.getNombreAtributo(), criterio.getValor());
				break;
			case ES_NULO:
				criterion = Restrictions.isNull(criterio.getNombreAtributo());
				break;
			case NO_ES_NULO:
				criterion = Restrictions.isNotNull(criterio.getNombreAtributo());
				break;
		default:
			break;			
		}
		return criterion;
	}
	
	protected Order convertirAOrder(CriterioConsulta criterio)
	{
		OperadoresConsulta operador = criterio.getOperador();
		Order orden = null;
		switch (operador)
		{
		case ORDENAR_POR_ASCENDENTEMENTE:
			orden = Order.asc(criterio.getNombreAtributo());
			break;
		case ORDENAR_POR_DESCENDENTEMENTE:
			orden = Order.asc(criterio.getNombreAtributo());
			break;
		default:
			break;
		}
		return orden;
	}

}
