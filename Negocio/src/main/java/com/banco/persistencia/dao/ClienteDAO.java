/**
 * 
 */
package com.banco.persistencia.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;

import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.persistencia.entidades.Cliente;

/**
 * @author david.mera
 *
 */
public class ClienteDAO extends DAO<Cliente> {

	public ClienteDAO() {
		super(Cliente.class);
	}

	public Criteria obtenerCriterios() {
		return getSession().createCriteria(this.getEntidad());
	}

	@SuppressWarnings("static-access")
	public void agregarCliente(Cliente cliente) throws ExcepcionRegistroExiste {
//		if (verificarClienteExiste(cliente).size() > 0)
//			throw new ExcepcionRegistroExiste("El cliente ya se encuentra registrado en la base de datos.");
//		else
		boolean excepcionIntegridad = false;
		try
		{
			this.agregarOrActualizar(cliente);
		}
		catch(org.hibernate.exception.ConstraintViolationException e)
		{
			excepcionIntegridad = true;
			throw new ExcepcionRegistroExiste(e.getMessage(), e);			
		}
		finally
		{
			if(excepcionIntegridad)
				this.closeSession();
		}
	}

	public void actualizarCliente(Cliente clienteViejo, Cliente clienteNuevo) throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste {
		List<Cliente> clientes = verificarClienteExiste(clienteViejo);
		int coincidencias = clientes.size();
		if (coincidencias == 0)
			throw new ExcepcionRegistroNoExiste("El cliente no se encuentra registrado en la base de datos.");
		else if (coincidencias == 1) {
			if(clienteNuevo.getId() == null || clienteNuevo.getId() == 0)
			{
				clienteViejo = clientes.get(0);
				clienteViejo.setApellido(clienteNuevo.getApellido());
				clienteViejo.setDireccion(clienteNuevo.getDireccion());
				//clienteViejo.setIdentificacion(clienteNuevo.getIdentificacion()); inmutable
				clienteViejo.setNombre(clienteNuevo.getNombre());
				clienteViejo.setTelefono(clienteNuevo.getTelefono());
				//
				clienteViejo.setCuentas(clienteNuevo.getCuentas());
				//
				this.agregarOrActualizar(clienteViejo);
			}
			else
				this.agregarOrActualizar(clienteNuevo);
			

		} else
			throw new ExcepcionRegistroExiste("El cliente se encuentra registrado más de una vez en la base de datos.");
		return;
	}

	private List<Cliente> verificarClienteExiste(Cliente cliente) {
//		ArrayList<Criterion> criterios = new ArrayList<Criterion>();
//		criterios.add(Restrictions.eq("nombre", cliente.getNombre()));
//		criterios.add(Restrictions.eq("apellido", cliente.getApellido()));
//		criterios.add(Restrictions.eq("identificacion", cliente.getIdentificacion()));
		
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
//		criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, cliente.getNombre()));
//		criterios.add(new CriterioConsulta("apellido", OperadoresConsulta.IGUAL, cliente.getApellido()));
		criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, cliente.getIdentificacion()));
		return (List<Cliente>) this.buscarClientesPor(criterios.toArray(new CriterioConsulta[criterios.size()]));
	}

	public void eliminarCliente(Cliente cliente) throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste {
		List<Cliente> clientes = verificarClienteExiste(cliente);
		int coincidencias = clientes.size();
		if (coincidencias == 0)
			throw new ExcepcionRegistroNoExiste("El cliente no se encuentra registrado en la base de datos.");
		else if (coincidencias == 1) {
			this.eliminar(clientes.get(0));
		} else
			throw new ExcepcionRegistroExiste("El cliente se encuentra registrado más de una vez en la base de datos.");
		return;
	}
	
	public List<Cliente> obtenerTodosClientes()
	{
		return this.obtenerTodos();
	}
	
	public Cliente obtenerClientePorId(Long id)
	{		
		return this.buscarPorId(id);
	}
			
	public List<Cliente> buscarClientesPor(CriterioConsulta... criterios)
	{
		return this.buscarPor(criterios);
	}
	
	public static Cliente cargarCuentasLazy(Cliente cliente)
	{
		getSession().update(cliente);
		Hibernate.initialize(cliente.getCuentas());
		closeSession();
		return cliente;
	}
	
}