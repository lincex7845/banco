/**
 * 
 */
package com.banco.persistencia.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;

import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.persistencia.entidades.Cuenta;

/**
 * @author david.mera
 *
 */
public class CuentaDAO extends DAO<Cuenta> {

	public CuentaDAO() {
		super(Cuenta.class);
	}

	public Criteria obtenerCriterios() {
		return getSession().createCriteria(this.getEntidad());
	}

	@SuppressWarnings("static-access")
	public void agregarCuenta(Cuenta nuevaCuenta) throws ExcepcionRegistroExiste {
		try {
			this.agregarOrActualizar(nuevaCuenta);
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			throw new ExcepcionRegistroExiste(e.getMessage(), e);
		} finally {
			this.closeSession();
		}
	}

	public void eliminarCuenta(String numeroCuenta) throws ExcepcionRegistroNoExiste, ExcepcionRegistroExiste
	{
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
		criterios.add(new CriterioConsulta("numero", OperadoresConsulta.IGUAL, numeroCuenta));
		List<Cuenta> cuentas = this.buscarPor(criterios.toArray(new CriterioConsulta[criterios.size()]));
		int coincidencias = cuentas.size();
		if (coincidencias == 0)
			throw new ExcepcionRegistroNoExiste("La cuenta no se encuentra registrado en la base de datos.");
		else if (coincidencias == 1) {
			this.eliminar(cuentas.get(0));
		} else
			throw new ExcepcionRegistroExiste("La cuenta se encuentra registrado más de una vez en la base de datos.");
			return;
	}
	
	public List<Cuenta> obtenerCuenta(CriterioConsulta... criterios)
	{
		//return this.buscarPor(criterios);
		Criteria crit = obtenerCriterios();
		for(CriterioConsulta criterio : criterios)
		{
			OperadoresConsulta operador = criterio.getOperador();
			if(operador == OperadoresConsulta.ORDENAR_POR_ASCENDENTEMENTE ||
					operador == OperadoresConsulta.ORDENAR_POR_DESCENDENTEMENTE)
				crit.addOrder(convertirAOrder(criterio));
			else
				crit.add(convertirACriterion(criterio));
		}
		@SuppressWarnings("unchecked")
		List<Cuenta> list = (List<Cuenta>)crit.list();
		Set<Cuenta> unique = new HashSet<Cuenta>(list);
		list.clear();
		list.addAll(unique);
		for(int i = 0; i < list.size(); i++)
			Hibernate.initialize(list.get(i).getMovimientos());
		closeSession();
		return list;
	}

	public void actualizarCuenta(Long idCuenta, Cuenta cuentaActualizada)
	{
		Cuenta c = this.buscarPorId(idCuenta);
		if(c != null)
		{
			this.agregarOrActualizar(cuentaActualizada);
		}
	}
}
