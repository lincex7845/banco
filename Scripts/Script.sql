-- Database: banco_db

-- DROP DATABASE banco_db;

CREATE DATABASE banco_db
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Spanish, Colombia'
       LC_CTYPE = 'Spanish, Colombia'
       CONNECTION LIMIT = -1;
GRANT CONNECT, TEMPORARY ON DATABASE banco_db TO public;
GRANT ALL ON DATABASE banco_db TO postgres;

CREATE USER banco_user WITH PASSWORD 'banco_Password#9';
GRANT ALL ON DATABASE banco_db TO banco_user;

