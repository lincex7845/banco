package com.banco.web.controladores;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.banco.infraestructura.excepciones.ExcepcionRegistroExiste;
import com.banco.infraestructura.excepciones.ExcepcionRegistroNoExiste;
import com.banco.infraestructura.utilidades.persistencia.CriterioConsulta;
import com.banco.infraestructura.utilidades.persistencia.OperadoresConsulta;
import com.banco.infraestructura.utilidades.validadores.StringValidator;
import com.banco.logicanegocio.BancoBL;
import com.banco.logicanegocio.ClienteDTO;
import com.banco.logicanegocio.CuentaDTO;
import com.banco.web.modelos.Cliente;

@Controller
public class ClienteController {
	
	@RequestMapping(value="/clientes", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public List<ClienteDTO> obtenerClientes(
			HttpServletRequest request,
            HttpServletResponse response,
            //@RequestBody final FiltroClientes filtro
            @RequestParam(value="identificacion", required=false)String identificacion,
            @RequestParam(value="nombre", required=false)String nombre,
            @RequestParam(value="apellido", required=false)String apellido)
	{
		List<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
		List<CriterioConsulta> criterios = new ArrayList<CriterioConsulta>();
		if(StringValidator.isValidString(identificacion))
			criterios.add(new CriterioConsulta("identificacion", OperadoresConsulta.IGUAL, identificacion));
		if(StringValidator.isValidString(nombre))
			criterios.add(new CriterioConsulta("nombre", OperadoresConsulta.IGUAL, nombre));
		if(StringValidator.isValidString(apellido))
			criterios.add(new CriterioConsulta("apellido", OperadoresConsulta.IGUAL, apellido));
		clientes = BancoBL.consultarClientes(criterios.toArray(new CriterioConsulta[criterios.size()]));
		if(clientes.size() < 1)
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		response.setStatus(HttpServletResponse.SC_OK);
		return clientes;
	}
	
	@RequestMapping(value="/clientes", method=RequestMethod.POST, consumes="application/json", produces="application/json")
	@ResponseBody
	public String agregarCliente(
			HttpServletRequest request,
            HttpServletResponse response,
            @RequestBody final Cliente cliente
			)
	{
		String retorno = "";
		response.setStatus(HttpServletResponse.SC_CREATED);
		try {
			CuentaDTO cuenta = new CuentaDTO(null, cliente.getNumeroCuenta(), 0L, null);
			ClienteDTO clienteNuevo = new ClienteDTO(
					null, 
					cliente.getNombre(), 
					cliente.getApellido(), 
					cliente.getIdentificacion(), 
					cliente.getDireccion(), 
					cliente.getTelefono(),
					java.util.Arrays.asList(cuenta));
			BancoBL.crearCliente(clienteNuevo);
			retorno = "Cliente creado exitosamente";
		} catch (ExcepcionRegistroExiste e) {
			retorno = e.getMessage();
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		}
		catch(Exception e)
		{
			retorno = e.getMessage();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return retorno;
	}
	
	@RequestMapping(value="/clientes/{idCliente}", method=RequestMethod.DELETE, produces="application/json")
	@ResponseBody
	public String eliminarCliente(
			HttpServletRequest request,
            HttpServletResponse response,
			@PathVariable Long idCliente)
	{
		String retorno = "";
		try {
			BancoBL.eliminarCliente(idCliente);
			retorno = "El cliente fue eliminado exitosamente.";
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (ExcepcionRegistroNoExiste e) {
			retorno = e.getMessage();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		} catch (ExcepcionRegistroExiste e) {
			retorno = e.getMessage();
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		}
		catch(Exception e)
		{
			retorno = e.getMessage();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return retorno;
	}
	
}
