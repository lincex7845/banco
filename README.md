# README #

Este archivo especifica estructura de la solucion

### Capas ###

* API Banco (carpeta Negocio)
    Bibliotecas de clases que contiene funcionalidades del banco. Se compone de los siguientes paquetes
    * infraestructura: componente trasversal que aporta utilidades (com.banco.infraestructura.utilidades.*) y excepciones (com.banco.infraestructura.excepciones)
    * loginanegocio: (com.banco.logicanegocio) componente que define las operaciones del dominio del problema, por ejemplo, el procedimiento para realizar movimientos en una cuenta. Implementa componente de persistencia
    * persistencia: componente que proporciona funcionalidades para operar contra la base de datos del banco (com.banco.persistencia.dao) y manejo de entidades de datos (com.banco.persistencia.entidades)
* Presentacion
    Implementa el API anteriormente mencionada. Consiste de un proyecto Web Spring MVC. Se compone de los siguientes paquetes
    * configuracion: Clases que permiten configurar e inicializar el servlet. 
    * controladores: Clases que proporcionan logica de negocio para la aplicación
    * modelos: entidades propias de la aplicacion

### Requerimientos ###

* JDK 1.7 o superior
* Eclipse
* Postgresql 9.1 o superior
* Maven 3
* Tomcat 7 (Puede ser instalado desde eclipse)

### Ejecucion ###

1. Una vez instalado Postgresql, se procede a ejecutar el script de creacion de base de datos (carpeta Scripts)
En tiempo de ejecución, el componente API creará las tablas.

2. Ubicad@ en la carpeta *Negocio* desde linea de comando o eclipse se procede a ejecutar maven con los goal *clean install*. Como resultado se creará un jar de nombre *negocio-api-1.0-jar-with-dependencies* el cual permitirá implementar las funcionalidades del banco desde aplicaciones de escritorio o web.

3. Ubicad@ en la carpeta *Presentacion/appweb* ejecutar maven con los goal *clean install*. Como resultado creará un war de nombre *Banco-web* que puede ser desplegado en Tomcat 7. Este proyecto requiere instalar/copiar el jar *negocio-api-1.0-jar-with-dependencies* en la carpeta *Presentacion/appweb/lib*

4. Se compia el archivo war en la carpeta *webapps* de Tomcat, se inicia el servicio a través del script *startup*, y se accede en un navagador a la ruta http://localhost:8080/Banco-Web